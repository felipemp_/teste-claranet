﻿using System.ComponentModel.DataAnnotations;

namespace Claranet.Models
{
    public class ClienteModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Cnpj")]
        public string cnpj { get; set; }

        [Display(Name = "Razao social")]
        public string razaoSocial { get; set; }

        [Display(Name = "Nome fantasia")]
        public string nomeFantasia { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Telefone")]
        public string telefone { get; set; }

        [Display(Name = "Telefone Comercial")]
        public string telComercial { get; set; }
        
        [Display(Name = "Celular")]
        public string celular { get; set; }

        [Display(Name = "Logradouro")]
        public string logradouro { get; set; }

        [Display(Name = "numero")]
        public string numero { get; set; }

        [Display(Name = "Complemento")]
        public string complemento { get; set; }

        [Display(Name = "Bairro")]
        public string bairro { get; set; }

        [Display(Name = "Cidade")]
        public string cidade { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        [Display(Name = "Cep")]
        public string cep { get; set; }

        [Display(Name = "Nome Contato")]
        public string nomeContato { get; set; }

        [Display(Name = "Senha")]
        public string senha { get; set; }
    }
}
