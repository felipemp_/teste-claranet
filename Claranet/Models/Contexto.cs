﻿using Microsoft.EntityFrameworkCore;

namespace Claranet.Models
{
    public class Contexto : DbContext
    {
        public DbSet<ClienteModel> Clientes { get; set; }

        public Contexto(DbContextOptions<Contexto> db) : base(db)
        {

        }
    }
}
