﻿using Claranet.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Claranet.Controllers
{
    public class CadastroController : Controller
    {
        private readonly Contexto _Contexto;

        public CadastroController(Contexto contexto)
        {
            _Contexto = contexto;
        }
        public IActionResult cadastroCliente()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> consultaCliente(int id)
        {
          ClienteModel cliente = await _Contexto.Clientes.FindAsync(id);
            return View(cliente);   
        }

        [HttpPost]
        public async Task<IActionResult> AddCliente(ClienteModel cliente)
        {
            await _Contexto.Clientes.AddAsync(cliente);
            await _Contexto.SaveChangesAsync();
            return RedirectToAction("index", "home");

        } 
    }
}
